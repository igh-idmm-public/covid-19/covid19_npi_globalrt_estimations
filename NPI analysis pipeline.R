rm(list=ls(all=TRUE)) 

# set Work directory
setwd("C:/Users/esra/Desktop/HE2RO/Control Measure Analysis/Data")

# Packages -----------------------------------------------------------------
require(data.table, quietly = TRUE) 
require(future, quietly = TRUE)
require(forecastHybrid, quietly = TRUE)
require(EpiNow, quietly = TRUE)
require(NCoVUtils, quietly = TRUE)
require(ggplot2, quietly = TRUE)
require(dplyr, quietly = TRUE)
require(reshape2, quietly = TRUE)
require(tidyverse, quietly = TRUE)
require(psych, quietly = TRUE)
require(lubridate, quietly = TRUE)
require(lme4, quietly = TRUE)
require(nlme, quietly = TRUE)
require(emmeans, quietly = TRUE)

# Load cases ---------------------------------------------------------------
cases <- read.csv("global_cases.csv", sep = ",", stringsAsFactors = FALSE)
cases$date <- as.Date(cases$date, "%d/%m/%Y")

# filter for countries in timeline dataset
timeline <- read.csv("analysis_timeline.csv", stringsAsFactors = FALSE)
countries <- levels(as.factor(timeline$country_name))
cases <- filter(cases, region %in% countries)

# Get linelist ------------------------------------------------------------

linelist <- data.table::fread("https://raw.githubusercontent.com/epiforecasts/NCoVUtils/master/data-raw/linelist.csv")
delays <- linelist[!is.na(date_onset_symptoms)][, 
                                                .(report_delay = as.numeric(lubridate::dmy(date_confirmation) - 
                                                                              as.Date(lubridate::dmy(date_onset_symptoms))))]
delays <- delays$report_delay

# Set up cores -----------------------------------------------------
if (!interactive()){
  options(future.fork.enable = TRUE)
}

future::plan("multiprocess", gc = TRUE, earlySignal = TRUE)

# Fit the reporting delay -------------------------------------------------

delay_defs <- EpiNow::get_dist_def(delays,
                                   bootstraps = 100, 
                                   samples = 1000)

# Fit the incubation period -----------------------------------------------

## Mean delay
exp(EpiNow::covid_incubation_period[1, ]$mean)

## Get incubation defs
incubation_defs <- EpiNow::lognorm_dist_def(mean = EpiNow::covid_incubation_period[1, ]$mean,
                                            mean_sd = EpiNow::covid_incubation_period[1, ]$mean_sd,
                                            sd = EpiNow::covid_incubation_period[1, ]$sd,
                                            sd_sd = EpiNow::covid_incubation_period[1, ]$sd_sd,
                                            max_value = 30, samples = 1000)


# Run regions nested ------------------------------------------------------

cores_per_region <- 1
future::plan(list(tweak("multiprocess", 
                        workers = floor(future::availableCores() / cores_per_region)),
                  tweak("multiprocess", workers = cores_per_region)),
             gc = TRUE, earlySignal = TRUE)

# Run pipeline ----------------------------------------------------

# define target directory
target_dir <- file.path("C:/Users/esra/Desktop/HE2RO/Control Measure Analysis/Data/EpiNow")

EpiNow::regional_rt_pipeline(
  cases = cases,
  delay_defs = delay_defs,
  incubation_defs = incubation_defs,
  target_folder = target_dir,
  case_limit = 60,
  horizon = 14,
  nowcast_lag = 14,
  approx_delay = TRUE,
  target_date = "2020-06-15"
)


future::plan("sequential")

# Summarise results -------------------------------------------------------

df2 <- read.csv("timeline_startdates.csv", stringsAsFactors = FALSE)
df3 <- df2 %>% mutate(
  start_date = as.Date(start_date, "%d/%m/%Y"), 
  end_date = as.Date(end_date, "%d/%m/%Y")) %>%
  filter(country_name != "Georgia")

time_varying_est <- data.frame()
nowcast <- data.frame()
x <- levels(as.factor(df3$country_name))

for (i in 1:length(x)) {
  key  <- x[i]
  rt <- "C:/Users/esra/Desktop/HE2RO/Control Measure Analysis/Data/EpiNow/REGION/latest/time_varying_params.rds"
  ncast <- "C:/Users/esra/Desktop/HE2RO/Control Measure Analysis/Data/EpiNow/REGION/latest/nowcast.rds"
  a <- gsub("REGION", key, rt)
  b <- gsub("REGION", key, ncast)
  c <- readRDS(a)
  e <- readRDS(b)
  d <- c$R0
  d$region <- key
  e$region <- key
  
  time_varying_est <- bind_rows(time_varying_est, d)
  nowcast <- bind_rows(nowcast, e)
  print(key)
  
}


rngLookup<-function(data,lookup){
  for(j in 1:nrow(lookup)){
    minval <- lookup[j, "start_date"]
    maxval <- lookup[j, "end_date"]
    label1 <- lookup[j, "intervention_group"]
    label2 <- lookup[j, "seq"]
    
    
    
    for(k in 1:nrow(data)){
      if(data[k, "date"] >= minval & data[k, "date"] < maxval){
        data[k, "intervention_group"] <- label1
        data[k, "seq"] <- label2
        
      }
    }
  }
  data
}



intervention_rt <- data.frame()
full_df <- data.frame()

for (i in 1:length(x)){
  key  <- x[i]
  a <- time_varying_est %>% filter(region == key) %>% mutate(country_name = region, region = NULL)
  b <- df3 %>% filter(country_name == key) %>% mutate(seq = row_number())
  c <- cases %>% filter(region == key, import_status == "local") %>%
    arrange(date) %>% distinct() %>%
    as.data.frame() %>% mutate(cum_cases = cumsum(confirm)) 
  
  
### CALCULATE % change in Rt ########################
  
  d <- b %>% inner_join(a,  by = c("start_date" = "date")) %>%
    rename(rt_t_start = median) %>%
    inner_join(a,  by = c("end_date" = "date")) %>%
    rename(rt_t_end = median) %>%
    mutate(per_change_Rt = (rt_t_end - rt_t_start)/rt_t_start) %>%
    select(start_date, end_date,seq, intervention_group, country_name, region, rt_t_start, rt_t_end, per_change_Rt, duration)
  
  e <-rngLookup(data=a,lookup= b)
  f <- e %>% mutate(intervention_group = as.character(intervention_group), 
                    intervention_group = tidyr::replace_na(intervention_group, "no_intervention")) %>%
    mutate(event_val = -200)
  
### CALCULATE ave Rt for each intervention period ########################
  g <- f %>% group_by(intervention_group, seq) %>%
    summarise(mean = mean(median, na.rm = TRUE),
              sd = sd(median, na.rm = TRUE),
              n = n()) %>%
    mutate(se = sd / sqrt(n),
           lower.ci = mean - qt(1 - (0.05 / 2), n- 1) * se,
           upper.ci = mean + qt(1 - (0.05 / 2), n - 1) * se) %>%
    ungroup() %>%
    as.data.frame() %>% select(intervention_group,seq,mean.rt = mean, lower.ci, upper.ci)
  
# Append to final datasets
  print(key)
  h <- c %>%  filter(cum_cases >= 50) 
  h$t_start <- row.names(h)
  h <- h %>% select(date, t_start)
  
  i <- f %>% left_join(g) %>% left_join(c)
  j <- d %>% left_join(g) %>% left_join(h, by = c("start_date" = "date")) %>%
    mutate_at(vars(t_start),  tidyr::replace_na, '0')
  full_df <- bind_rows(full_df, i)
  intervention_rt <- bind_rows(intervention_rt, j) #append to final dataset
  
  print(key)
  
}
  
# Regression ------------------------------------------------------- 


reg_levels = c("household_confined","quar_iso","social_group_limits",
               "school_closed","mask","restrictions_eased")

reg_df <- intervention_rt %>% rename(median_rt = mean.rt) %>%
  filter(duration > 5) %>%
  mutate(intervention_group = factor(intervention_group, levels = reg_levels), 
         t_start = as.numeric(t_start)) 


##PERIOD SPECIFIC Rt Regression
reg <- lme(per_change_Rt ~ intervention_group + t_start, random=~1|country_name, data = reg_df)
anova(reg)
summary(reg)
intervals(reg, which = "fixed")
emmeans(reg, "intervention_group")
emmeans(reg, "t_start")

# Results Vis ------------------------------------------------------- 

intervention_levels = c("quar_iso","social_group_limits","school_closed","mask", 
                        "household_confined","restrictions_eased")

cols <- c( quar_iso = "#E87700",
           social_group_limits = "#EACB2B", 
           school_closed = "#9FC095", 
           mask = "#3B99B1", 
           household_confined = "#F5191C", 
           restrictions_eased ="slategrey") 

visreg_df <-  reg_df %>%
  mutate(intervention_group = factor(intervention_group, levels = intervention_levels))


p1 <- ggplot(visreg_df, aes(x = t_start , y = median_rt, colour = intervention_group)) +
  geom_point(size = 2) +
  theme(aspect.ratio = 1)+
  theme_classic() +
  scale_colour_manual(values = cols) +
  theme(legend.position = "none") +
  xlab("days since cases > 50") +
  ylab("median daily Rt")
p1

p2 <- ggplot(visreg_df, aes(x = t_start , y = per_change_Rt*100, colour = intervention_group)) +
  geom_point(size = 2) +
  theme(aspect.ratio = 1)+
  theme_classic() +
  scale_colour_manual(values = cols) +
  theme(legend.position = "none") +
  xlab("days since cases > 50") +
  ylab("% change in daily Rt") 
p2

p3 <- ggplot(visreg_df, aes(x = intervention_group, y= per_change_Rt*100, fill = intervention_group)) +
  geom_boxplot() +
  theme_classic() +
  theme(axis.text.x = element_blank(), aspect.ratio = 1)+
  scale_fill_manual(values = cols) +
  xlab("Non-pharmaceutical intervention") +
  ylab("% change in daily median Rt") +
  theme(legend.position = "none")

p3


tiff("fig2.tiff",units = "in", width = 7, height = 7, res=300)
cowplot::plot_grid(p1, p2,p3, labels = c('A', 'B', 'C'), label_size = 12)
dev.off()

# Timeline Vis ------------------------------------------------------- 

region_map <- timeline %>% group_by(country_name, region) %>%
  summarise(n = n())%>%
  dplyr::select(country_name, region) %>%
  ungroup() %>% as.data.frame() %>%
  select(country_name, area = region)

df7 <- full_df %>% left_join(region_map)

graph_cols <- c(no_intervention = "#FFFFFF", 
                quar_iso = "#E87700",
                social_group_limits = "#EACB2B", 
                school_closed = "#9FC095", 
                mask = "#3B99B1", 
                household_confined = "#F5191C", 
                restrictions_eased ="slategrey") 

# Non-US Countries  ------------------------------------------------------- 
global_levels <-  df7 %>% filter(area != "United States of America") %>%
  group_by(country_name)%>%
  summarise(max_cases = max(cum_cases, na.rm = TRUE)) %>%
  arrange(-max_cases)
g_order <- global_levels$country_name


g1 <-  df7 %>% filter(area != "United States of America") %>%
  filter(date < as.Date("2020-05-31"), date > as.Date("2020-02-15") ) %>%
  mutate(country_name = factor(country_name, levels = g_order)) %>%
  ggplot(aes(date)) +
  geom_col(aes( y=confirm), width = 1, fill = "grey79") +
  geom_col(aes( y=event_val, fill = intervention_group), width = 1) +
  scale_fill_manual(values=graph_cols) +
  scale_x_date(expand = c(0, 0)) +
  facet_wrap(~ country_name, ncol = 5) +
  theme(strip.text = element_text(size= 6),
        strip.background = element_rect(fill="lightblue",size=1))
g1

g2 <- g1 +
  geom_line(aes(y=median*3000), colour="slategray4") + 
  geom_ribbon(aes(ymin=lower*3000, ymax=upper*3000), alpha=0.5, fill= "slategray2")+
  theme(aspect.ratio = 1)
g2

g3 <- g2 + scale_y_continuous(sec.axis = sec_axis(~ . /3000, name = "Daily Rt"), limits=c(-200,9000))
g4 <- g3 + theme(legend.position = "none", 
                 axis.text=element_text(size=6))+
  ylab("Daily Reported Cases")+
  xlab("") +
  ggtitle("A")
g4

tiff("fig1A.tiff",units = "in", width = 7, height = 7, res=300)
g4
dev.off() 

# US States ------------------------------------------------------- 

us_levels <-  df7 %>% filter(area == "United States of America") %>%
  group_by(country_name)%>%
  summarise(max_cases = max(cum_cases, na.rm = TRUE)) %>%
  arrange(-max_cases)
us_order <- us_levels$country_name

g1 <-  df7 %>% filter(area == "United States of America") %>%
  filter(date < as.Date("2020-05-31"), date > as.Date("2020-02-15") ) %>%
  mutate(country_name = factor(country_name, levels = us_order)) %>%
  ggplot(aes(date)) +
  geom_col(aes( y=confirm), width = 1, fill = "grey79") +
  geom_col(aes( y=event_val, fill = intervention_group), width = 1) +
  scale_x_date(expand = c(0, 0)) +
  scale_fill_manual(values=graph_cols) +
  facet_wrap(~ country_name, ncol = 5) +
  theme(strip.text = element_text(size= 6),
        strip.background = element_rect(fill="lightblue",size=1))
g1

g2 <- g1 +
  geom_line(aes(y=median*4000), colour="slategray4") + 
  geom_ribbon(aes(ymin=lower*4000, ymax=upper*4000), alpha=0.5, fill= "slategray2")+
  theme(aspect.ratio = 1)
g2

g3 <- g2 + scale_y_continuous(sec.axis = sec_axis(~ . /4000, name = "Daily Rt"), limits=c(-200,12000))
g4 <- g3 + theme(legend.position = "none", 
                 axis.text=element_text(size=6))+
  ylab("Daily Reported Cases")+
  xlab("") +
  ggtitle("B")
g4

tiff("fig1B.tiff",units = "in", width = 7, height = 7, res=300)
g4
dev.off()




  

